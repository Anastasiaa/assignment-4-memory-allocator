#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool blocks_continuous ( struct block_header const* fst, struct block_header const* snd );


static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t size = region_actual_size (query + offsetof(struct block_header, contents) );
    void* mapped = map_pages(addr, size, MAP_FIXED_NOREPLACE);
    if (MAP_FAILED == mapped) {
        mapped = map_pages(addr, size, 0);
        if (MAP_FAILED == mapped) return REGION_INVALID;
    }
    block_init(mapped, (block_size) { .bytes = size }, NULL);
    return (struct region) {
            .addr = MAP_FAILED == mapped ? NULL : mapped,
            .size = size,
            .extends = mapped == addr
    };

}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    void* region_addr = (struct block_header*) HEAP_START;
    while (region_addr != NULL) {
        struct block_header* current_addr = region_addr;
        size_t size = size_from_capacity(current_addr->capacity).bytes;
        while (blocks_continuous(current_addr, current_addr->next)) {
            current_addr = current_addr->next;
            size += size_from_capacity(current_addr->capacity).bytes;
        }
        current_addr = current_addr->next;
        munmap(region_addr, size);
        region_addr = current_addr;
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block && block_splittable(block, query)) {
        query = size_max(query, BLOCK_MIN_CAPACITY);
        void* addr = block->contents + query;
        block_init(addr, (block_size){.bytes = block->capacity.bytes - query}, block->next);
        block->capacity.bytes = query;
        block->next = addr;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (!block) {
        return false;
    }
    struct block_header *next_block = block->next;
    if (next_block && mergeable(block, next_block))
    {
        block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
        block->next = next_block->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if (!block) {
        return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
    }
    struct block_header* last_block;
    do {
        if (block->is_free){
            while(try_merge_with_next(block));
            if (block_is_big_enough(sz, block)) {
                return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = block};
            }
        }
        last_block = block;
        block = block->next;
    } while (block);
    return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = last_block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result founded_block = find_good_or_last(block, query);
    if (BSR_FOUND_GOOD_BLOCK == founded_block.type) {
        if (split_if_too_big(founded_block.block, query)) {
            while(try_merge_with_next(founded_block.block->next));
        }
        founded_block.block->is_free = false;
    }
    return founded_block;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    const void* next_block_addr = block_after(last);
    if (!last) {
        return NULL;
    }
    const struct region region = alloc_region(next_block_addr, query);
    if (region_is_invalid(&region)) {
        return NULL;
    }
    last->next = region.addr;
    if (next_block_addr == region.addr && last->is_free) {
        return try_merge_with_next (last) ? last : NULL;
    }
    return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    struct block_search_result res = try_memalloc_existing(query, heap_start);
    if (res.type == BSR_FOUND_GOOD_BLOCK) {
        return res.block;
    }
    if (res.type == BSR_REACHED_END_NOT_FOUND) {
        grow_heap(res.block, query);
        res = try_memalloc_existing(query, heap_start);
        if (res.type == BSR_FOUND_GOOD_BLOCK) {
            return res.block;
        }
    }
    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    while (try_merge_with_next(header));
}
